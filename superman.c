#include "types.h"
#include "constants.h"
#include "intrinsics.h"
#include "natives.h"
#include "common.h"

// prints a basic message to the kill feed
void printFeed(const char* msg)
{
	_SET_NOTIFICATION_TEXT_ENTRY("STRING");
	ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(msg);
	int useless = _DRAW_NOTIFICATION(0, 1);
}

void main()
{
	printFeed("~b~L2~w~: Airbrake~n~~b~R2~w~: Accelerate~n~");
	printFeed("Tap ~b~R1~w~ while jumping to launch");

	while (true)
	{
		Ped playerPed = PLAYER_PED_ID();
		vector3 zero = { 0.0f, 0.0f, 0.0f };

		if (!HAS_PED_GOT_WEAPON(playerPed, GADGET_PARACHUTE, 0))
		{
			GIVE_DELAYED_WEAPON_TO_PED(playerPed, GADGET_PARACHUTE, 99999, true);
		}
		if (GET_PED_PARACHUTE_STATE(playerPed) == 0)
		{
			vector3 force;

			// get L2 R2 and left analog values and convert range of 0 to 254 to range of -127 to 127
			int L2 = GET_CONTROL_VALUE(0, INPUT_SCRIPT_LT) - 127;
			int R2 = GET_CONTROL_VALUE(0, INPUT_SCRIPT_RT) - 127;
			int LY = GET_CONTROL_VALUE(0, INPUT_SCRIPT_LEFT_AXIS_Y) - 127;
			int LX = GET_CONTROL_VALUE(0, INPUT_SCRIPT_LEFT_AXIS_X) - 127;

			// steering
			float turn = 0.0039f * (float)LX;

			// cannot apply a rotation force without a directional force (I think?)
			// so apply offsetting directional forces that have compounding rotational force
			APPLY_FORCE_TO_ENTITY(playerPed, 1, (vector3) { 0.0f, 3.0f, 0.0f }, (vector3) { -turn, 0.0f, 0.0f }, 0, 1, 1, 1, 1, 1);
			APPLY_FORCE_TO_ENTITY(playerPed, 1, (vector3) { 0.0f, -3.0f, 0.0f }, (vector3) { turn, 0.0f, 0.0f }, 0, 1, 1, 1, 1, 1);

			// flying
			if (L2 > 0)// brakes applied - apply only non-relative force using inverse velocity and multipliers
			{
				vector3 vel = GET_ENTITY_VELOCITY(playerPed);

				// analog multiplier - for readability, can be compressed into shit below
				float brake = 0.0039f * (float)L2;

				force.x = -vel.x * brake;
				force.y = -vel.y * brake;
				force.z = (-vel.z * brake) + 0.25f;// 0.25f vertical bump to stop slow falling

				// apply calculated force
				APPLY_FORCE_TO_ENTITY(playerPed, 1, force, zero, 0, 0, 1, 1, 1, 1);
			}
			else// regular superman - force is relative - multipliers are confusing
			{
				force.x = 0.0f;
				force.y = 0.0f + ((float)R2 * 0.0833f);
				force.z = 0.37f + ((float)LY * 0.0111f);

				// extra upward force if analog is being pulled back
				if (LY > 0)
					force.z = force.z + force.y;

				// apply calculated force
				APPLY_FORCE_TO_ENTITY(playerPed, 1, force, zero, 0, 1, 1, 1, 1, 1);
			}
		}
		else
		{
			if (IS_PED_JUMPING(playerPed))// vertical boost while jumping (doesn't work in shallow water or mud?)
			{
				if (IS_CONTROL_PRESSED(0, INPUT_FRONTEND_RB))
				{
					APPLY_FORCE_TO_ENTITY(playerPed, 1, (vector3) { 0.0f, 0.0f, 70.0f }, zero, 0, 0, 1, 1, 1, 1);
					TASK_PARACHUTE(playerPed, true);
				}
			}
			else if (IS_PED_SWIMMING(playerPed))// attempted vertical boost out of water (fails miserably)
			{
				if (IS_CONTROL_PRESSED(0, INPUT_FRONTEND_RB) && IS_CONTROL_PRESSED(0, INPUT_FRONTEND_X))
				{
					APPLY_FORCE_TO_ENTITY(playerPed, 1, (vector3) { 0.0f, 0.0f, 70.0f }, zero, 0, 0, 1, 1, 1, 1);
					TASK_PARACHUTE(playerPed, true);
				}
			}
		}
		WAIT(0);
	}
}
